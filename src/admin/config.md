# Configuring Veilid

## The configuration file

The Veilid headless server (veilid-server) is configured by use of a configuration file, in YAML format.

### The YAML format (as much as we need to know)

YAML stands for "YAML Ain't Markup Language". It is an indentation-sensitive markup language intended for easy understanding of setting groups, setting names, and their values. Number values are not quoted, and string values are single-quoted. There are two forms, "group" and "flattened".

In group form, every group is indented by two spaces. For example:

```yaml
network:
  routing_table:
    node_id: 'VLD0:AAAAAAAABBBBBBBBccccccccDDDDDDDDeeeeeeeeFFf'
```

In flattened form, all of the components of the setting group and setting name are flattened into a single line, separated by '.' characters.

The example above would correspond to the following line:

```yaml
network.routing_table.node_id: 'VLD0:AAAAAAAABBBBBBBBccccccccDDDDDDDDeeeeeeeeFFf'
```

For more comprehensive information about YAML, please see [The YAML Homepage](https://yaml.org/).

### Configuration Keys

| Key Name                                          | Value Type | What It Does |
|---------------------------------------------------|------------|--------------|
| daemon.enabled | Boolean | veilid-server will run in the background as a daemon. |
| client_api.enabled | Boolean | veilid-server will respond to Python and other JSON client requests. |
| client.listen_address | String | The address and port the client API will listen on. |
| auto_attach | Boolean | veilid-server will automatically attach to the Veilid network. |
| logging.system.enabled | Boolean | Events of type 'system' will be logged. |
| logging.system.level | String | The minimum priority of system events to be logged. |
| logging.terminal.enabled | Boolean | Events of type 'terminal' will be logged. |
| logging.terminal.level | String | The minimum priority of terminal events to be logged. |
| logging.api.enabled | Boolean | Events of type 'api' will be logged. |
| logging.api.level | String | The minimum priority of api events to be logged. |
| logging.console.enabled | Boolean | Logged events will be printed to the console. |
| logging.file.enabled | Boolean | Logged events will be stored in a log file. |
| logging.file.path | String | The filename that logged events will be stored in. |
| logging.file.append | Boolean | If true, the log file will not be truncated before writing logs to it. |
| logging.file.level | String | **** |
| logging.otlp.enabled | Boolean | If true, Open Telemetry (OTLP) logging will be enabled. |
| logging.otlp.level | String | **** |
| logging.otlp.grpc_endpoint | String | The address of an OTLP/gRPC endpoint that will receive log messages. |
| testing.subnode_index | Number | For testing purposes, the subnode index of this instance of veilid-server. See [testing.subnode_index](subnode_index) for more information.]
| core.program_name | String | The name of your app (not "Veilid") |
| core.namespace | String | A specific name appended to the name of your app's keyring file, to support multiple apps on a single device |
| core.capabilities.disable | List | A list of capabilities to disable (for example, DHTV to say you cannot store DHT information) |
| core.table_store.directory | String | The filesystem directory to store your table store within |
| core.table_store.delete | Boolean | Whether to delete everything in the table store at startup |
| core.block_store.directory | String | \* The filesystem directory to store blocks for the block store |
| core.block_store.delete | Boolean | \* Whether to delete everything in your local block store at startup |
| core.protected_store.allow_insecure_fallback | Boolean | If we can't use system-provided secure storage, should we proceed anyway? |
| core.protected_store.always_use_insecure_storage | Boolean | Should we bypass any attempt to use system-provided secure storage? |
| core.protected_store.directory | String | The filesystem directory to store your protected store in |
| core.protected_store.delete | Boolean | Whether to delete everything in your protected store at startup |
| core.protected_store.device_encryption_key_password | String | The password to derive a key to encrypt the current protected store from |
| core.protected_store.new_device_encryption_key_password | String | A password to derive a key to change the encryption of the protected store to |
| core.network.connection_initial_timeout_ms | Integer | How long should we wait when trying to make an outbound connection? |
| core.network.connection_inactivity_timeout_ms | Integer | How long should a connection be inactive for before we close it? |
| core.network.max_connections_per_ip4 | Integer | How many connections can a particular IPv4 address block be able to make? | ***
| core.network.max_connections_per_ip6_prefix | Integer | How many connections can a particular IPv6 network be able to make? |
| core.network.max_connections_per_ip6_prefix_size | Integer | How many bits of address must match to be considered the same prefix? |
| core.network.max_connection_frequency_per_min | Integer | How many connections per minute are allowed from a particular IP block? |
| core.network.client_allowlist_timeout_ms | Integer | How long a particular remote node remains in the allowlist |
| core.network.reverse_connection_receipt_time_ms | Integer | When we send a DialInfo to get a reverse connection, how long should we wait for that connection to be received? |
| core.network.hole_punch_receipt_time_ms | Integer | How long to wait for a remote UDP hole punch to be made |
| core.network.network_key_password | String | The password to differentiate the veilid network to connect your app to |
| core.network.routing_table.node_id | String | Base64-encoded public key for the node, used as the node's ID |
| core.network.routing_table.node_id_secret | String | Base64-encoded private key corresponding to the node_id |
| core.network.routing_table.bootstrap | String | Host name of existing well-known Veilid bootstrap servers for the network to connect to |
| core.network.routing_table.limit_over_attached | Integer | How many network attachments (reliable nodes in the recently-used list) are considered "Overly Attached" |
| core.network.routing_table.limit_fully_attached | Integer | How many network attachments are considered "Fully Attached" |
| core.network.routing_table.limit_attached_strong | Integer | How many network attachments are considered "Strong Attachment" |
| core.network.routing_table.limit_attached_good | Integer | How many network attachments are considered "Good Attachment" |
| core.network.routing_table.limit_attached_weak | Integer | How many network attachments are considered "Weak Attachment" |
| core.network.dht.max_find_node_count | Integer | Maximum number of nodes to find on any given search |
| core.network.dht.resolve_node_timeout_ms | Integer | How long to wait for nodes to resolve, in milliseconds |
| core.network.dht.resolve_node_count | Integer | Maximum number of nodes that can be resolved in any request |
| core.network.dht.resolve_node_fanout | Integer | The number of nodes near the desired node to request for it |
| core.network.dht.get_value_timeout_ms | Integer | How long to wait for DHT values to be retrieved |
| core.network.dht.get_value_count | Integer | Maximum number of consensus values to get |
| core.network.dht.get_value_fanout | Integer | The number of nodes near the desired DHT record to request for it simultaneously |
| core.network.dht.set_value_timeout_ms | Integer | How long to wait for DHT values to be written |
| core.network.dht.set_value_count | Integer | How many nodes near the desired DHT key will we write it to |
| core.network.dht.set_value_fanout | Integer | The number of nodes near the desired DHT record to write it to simultaneously |
| core.network.dht.min_peer_count | Integer | Minimum number of nodes to keep in the peer table |
| core.network.dht.min_peer_refresh_time_ms | Integer | Minimum amount of time since we contacted a node to refresh its availability |
| core.network.dht.validate_dial_info_receipt_time_ms | Integer | How long receipts we generate for validate dial info requests are valid |
| core.network.dht.local_subkey_cache_size | Integer |
| core.network.dht.local_max_subkey_cache_memory_mb | Integer |
| core.network.dht.remote_subkey_cache_size | Integer |
| core.network.dht.remote_max_records | Integer |
| core.network.dht.remote_max_subkey_cache_memory_mb | Integer |
| core.network.dht.remote_max_storage_space_mb | Integer |
| core.network.rpc.concurrency | Integer | Number of concurrent RPC requests that can be outstanding. |
| core.network.rpc.queue_size | Integer | The maximum number of unprocessed RPCs (must be >= 256) |
| core.network.rpc.max_timestamp_behind_ms | Integer |
| core.network.rpc.max_timestamp_ahead_ms | Integer |
| core.network.rpc.timeout_ms | Integer | How long an RPC can be outstanding before it times out (must be >= 1000) |
| core.network.rpc.max_route_hop_count | Integer | Max number of hops in a route (must be <= 5) |
| core.network.rpc.default_route_hop_count | Integer | Default number of hops in a route (must be <= max_route_hop_count) |
| core.network.upnp | Boolean | Should the app try to improve its incoming network connectivity using UPnP? |
| core.network.detect_address_changes | Boolean | Should veilid-core detect and notify on network address changes? |
| core.network.restricted_nat_retries | Integer | How many tries to accept incoming connections before we decide we're on a restricted NAT? |
| core.network.tls.certificate_path | String | Filesystem path to a file containing the TLS certificate chain |
| core.network.tls.private_key_path | String | Filesystem path to a file containing the TLS private key |
| core.network.tls.connection_initial_timeout_ms | Integer | How long should we wait for a TLS handshake on an incoming connection? |
| core.network.application.https.enabled | Boolean | HTTPS is enabled (requires core.network.tls.* to be set) |
| core.network.application.https.listen_address | String | The interface and port for HTTPS to listen on |
| core.network.application.https.path | String | The URI path which must match for a request to be accepted |
| core.network.application.https.url | String | The full URL, including hostname (which must be certified in the certificate) which must match the request |
| core.network.application.http.enabled | Boolean |
| core.network.application.http.listen_address | String |
| core.network.application.http.path | String |
| core.network.application.http.url | String |
| core.network.protocol.udp.enabled | Boolean |
| core.network.protocol.udp.socket_pool_size | Integer |
| core.network.protocol.udp.listen_address | String |
| core.network.protocol.udp.public_address | String |
| core.network.protocol.tcp.connect | Boolean |
| core.network.protocol.tcp.listen | Boolean |
| core.network.protocol.tcp.max_connections | Integer |
| core.network.protocol.tcp.listen_address | String |
| core.network.protocol.tcp.public_address | String |
| core.network.protocol.ws.connect | Boolean |
| core.network.protocol.ws.listen | Boolean |
| core.network.protocol.ws.max_connections | Integer |
| core.network.protocol.ws.listen_address | String |
| core.network.protocol.ws.path | String |
| core.network.protocol.ws.url | String |
| core.network.protocol.wss.connect | Boolean |
| core.network.protocol.wss.listen | Boolean |
| core.network.protocol.wss.max_connections | Integer |
| core.network.protocol.wss.listen_address | String |
| core.network.protocol.wss.path | String |
| core.network.protocol.wss.url | String |
