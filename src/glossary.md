# Glossary Of Terms

attachment: The measure of how many connections a node currently has to the Veilid network.

safety route: A route designated by a sending node to protect its physical network connection.

secret route: A route designated by a receiving node to protect its physical network connection.

DHT: Distributed Hash Table, a key-value store which stores its keys and values across many nodes, and which provides a means to look them up. In Veilid, every DHT record has a key, a schema, a creator keypair, and subkeys with values. If it uses the Simple schema, it may also have writer keypairs for one or more of its subkeys.

dialinfo: The collection of a node's public key, location on the physical network, and physical protocols it can accept connections on.

callback function: A function which the application developer writes and which modifies the application state, which is provided to the Veilid library for it to call when network events happen.

Default schema: A DHT schema type which allows only the creator keypair to write to its value or any of its subkey values.

Simple schema: A DHT schema type which allows the creator keypair to delegate the ability to write to subkeys to some other keypair or keypairs.

Creator keypair: The keypair owned by the creator of a DHT record.

Writer keypair: A keypair owned by someone the creator of a DHT record has given the ability to write to a subkey or several subkeys.
