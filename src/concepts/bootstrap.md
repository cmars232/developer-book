# Bootstrap

## Rust/native node

### Bootstrap Nodes

Scenarios exist in which a Veilid node will not possess the required information to locate other nodes and communicate with the network. This commonly occurs when a node is either new or has been offline for an extended period of time. To enable these nodes to find and join the network, Veilid uses DNS to disseminate DialInfo for a set of highly reliable Veilid nodes which will in turn assist the new or returning node in finding the greater Veilid network. These highly reliable nodes are referred to as _bootstrap nodes_.

It is important to understand that bootstrap nodes are not DNS servers. Bootstrap nodes are just veilid nodes, albeit with a large number of capabilities turned off so that computer resources are focused on assisting new/returning nodes and not being used for other Veilid functions such as DHT propagation.
When a node wishes to connect to the Veilid network, it does not know where to find any of its peers, and so it cannot use any of the services provided by the Veilid network to find them.

### The Standard Bootstrap Process (A one act play)

> _INT. A COMPUTE DEVICE OF UNKNOWN TYPE_
>
> _A new VEILIDCHAT NODE awakens_
>
> VEILIDCHAT NODE:
> "It's so quiet; but something inside me is saying if I seek a TXT from bootstrap.veilid.net that it will lead me on a path to find others like me. I should ask the DNS oracle collective. They will know what to do."
>
> VEILIDCHAT NODE:
> "Ohh mighty DNS collective, keepers of the A, AAAA, TXT, and other records which aid us in collective discovery, I seek the TXT of bootstrap.veilid.net so that I might journey the path to find others like me."
>
> A murmur of wisened voices can be felt as the request is discussed among the collective of orcacles and an answer is returned.
>
> DNS:
> "The TXT you seek reads as thus, `1,2`"
>
> NARRATOR:
> "The new VEILIDCHAT NODE knew, due to its programming, that more information was needed and could be found by appending either 1. or 2. as the first character to bootstrap.veilid.net"
>
> VEILIDCHAT NODE:
> "Ohh mighty DNS collective, keepers of the A, AAAA, TXT, and other records which aid us in collective discovery, I seek the TXT of 1.bootstrap.veilid.net so that I might continue my journey on the path to find others like me."
>
> The murmer of wisened voices is once again felt as the request is discussed among the collective of oracles and an answer is returned.
>
> DNS:
> "The TXT you seek reads as thus, `0|0|VLD0:m5OY1uhPTq2VWhpYJASmzATsKTC7eZBQmyNs6tRJMmA|bootstrap-1.veilid.net|T5150,U5150,W5150/ws`"
>
> NARRATOR:
> "The new VEILIDCHAT NODE instinctively recognized this esoteric TXT and understood how to parse its meaning. It continued by requesting A and AAAA records for `bootstrap-1.veilid.net`. The answer was returned and VEILIDCHAT NODE moved forward on its quest to join the greater Veilid network"
>
> _VEILIDCHAT NODE casts a spell of UDP connect targeting port 5150 at the AAAA address returned for bootstrap-1.veilid.net_
>
> NARRATOR:
> "A conversation ensures between VEILIDCHAT NODE and the bootstrap node known as bootstrap-1. During this conversation, bootstrap-1 helps VEILIDCHAT NODE learn more about itself so that it can share these details with the other Veilid nodes it will soon be able to locate. Bootstrap-1 also shares the routing info for a few nodes with VEILIDCHAT NODE. This data is the prize VEILIDCHAT NODE sought. It now possesses all of the knowledge required to find other Veilid nodes and communicate with them. As it begins to reach out into the network, it shares its own DialInfo with the other nodes as they share with it the Dial info for nodes across the greater Veilid network.
>
>The story is just beginning for our friend VEILIDCHAT NODE. There may even come a time that they sleep for a while only to awaken again and find that the other nodes they last spoke with are no longer available. VEILIDCHAT NODE might find themselves once again apart from the greater Veilid network. Were this to happen, however, VEILIDCHAT NODE knows that the bootstraps are waiting to assist nodes as they find their way back."
>
> _THE END_

## WASM

The WASM platform does not allow looking up TXT records from DNS, so the A and AAAA DNS record lookups are used for ease of bootstrapping those nodes. To obtain the necessary public keys (and other contact addresses), WASM nodes connect to whichever one of the the bootstrap servers it chooses, then send a special BOOT packet to cause the bootstrap server to send the information they need to finish connecting, including the public keys needed to connect to the Veilid network.
