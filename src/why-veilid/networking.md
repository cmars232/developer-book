# Networking Topologies

When it comes to how centralized or distributed important network services are, there are three main networking topologies. Veilid uses the "Distributed/P2P" topology.

## Centralized

## Decentralized/Federated

The second topology is what's known as "decentralized", or "federated". In this form of network, several different administrations have decided to work together so that each one provides a smaller number of services under some information-sharing agreement that allows a single identity (login) to be used with all of them.

The most widely-used federated protocol, and the most benign, is called the Domain Name System, or DNS. Everyone shares the names of computers on their network that external people need to know about, to make sure that they can find them on the network.

Yet, even DNS data is revealing, and you can bet that your Internet provider (or whoever provides your DNS service) is watching it like a hawk. Perhaps you're using pinterest, or purchasing things from Etsy. Or buying things from Amazon, or watching videos on Youtube. Or watching videos on other, racier sites. Your Internet provider knows what you do online, and since they, too, are required to "enhance shareholder value" they're almost certainly scraping that data and selling it to data brokers.

For small services, being able to rely on another company's authentication is something that makes a great deal of sense. Why would they want to run their own authentication? Every account someone signs up for typically takes at least 2 minutes, often more like 5 minutes. Being able to skip that process is worth whatever they have to pay to make it possible. Plus, it's always desirable to avoid problems for many users in a data breach.

But the most widely-used federation protocol in use, OpenID, spills information to whoever provides the authentication, often in the form of "Referer:" (sic) headers. So, they know who you are, and what site you're asking to authenticate to. This is also a privacy nightmare, and usually you have no idea who they're selling that data to. Which, again, increases the level of detail that a dossier about you can include.

Also, large social media sites can use federation to sidestep the results of their moderation decisions -- "if we kicked you off, just find another server to use, and we won't know it's you so you can keep doing what we kicked you off for."

## Distributed/P2P

The final topology is known as "distributed", or "peer to peer". This is where individual people share their services with each other. Various services (Napster, Limewire, and Bittorrent) have been set up to facilitate this kind of sharing... but if you remember Napster, you know that even these services often have major architectural weaknesses. Public bittorrent trackers often end up being taken down, primarily due to legal action that copyright holders take. Napster was sued into liquidation and no longer exists, so there's no way for the peers who might want to use it to even find each other. (Yes, this means that Napster was actually centralized, even though they profited from users sharing information directly between each other, using the Napster service to find each other.)

Various attempts have been made to create truly peer to peer services. Perhaps the most famous of these is Tor, the Onion Router, which aims to anonymize your internet browsing by routing it through 2 or 3 computers who don't actually know each other (and, due to cryptography, actually can't know each other). Other projects include I2P (the Invisible Internet Project), ZeroNet, Freenet, and GNUnet.
