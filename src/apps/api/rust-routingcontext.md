# RoutingContext

A RoutingContext is how a node talks to other nodes.

Once a Veilid node is attached, it receives the fundamental routing information for the network. However, this doesn't allow for anonymity for either a sender or receiver, or for other communications preferences to be set. As such, a RoutingContext needs to be created.

A RoutingContext contains three things: 